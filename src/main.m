function [R_out, G_out, B_out, ampli_f, ampli_r]= fcn(R_in, G_in, B_in)
% Written by Ahmed Ghaleb and Aymeric Legros
img = cat(3, R_in, G_in, B_in);

[width, height] = size(R_in);


    % These variables are used to crop the image
    a = floor(width/4);
    b = floor((3*width)/4);
    c = floor(height/4);
    d = floor((3*height)/4);
    
    % Cropping the image
    ROI = img(a:b,c:d);

    % We apply median filter
    ROI = medfilt2(ROI);

    % Thresholding
    ROI(ROI < 190) = 0;

    % Black and white image computed with edges
    BW = edge(ROI,'canny');

    % Extracting the array in the middle of the image
    line = BW(a*2,:);

    edges = find(line);
    ampli_f = 0;
    ampli_r = 0;
    % If there are more than 2 edges in the image
    if length(edges) > 1

        % We compute the center of the image
        center = (a + b) / 2;

        % We calculate the deviation
        deviation = (center - (a + edges(1))) / (width / 4);
        ampli_f = 0;
        ampli_r = 0;
        if deviation > 0
         ampli_f = deviation*150;
        elseif deviation < 0
         ampli_r = -deviation*150;
        end
   end

% Converting the image to 8 bits
res = uint8(ones([240,320]));

% This is the result image (it's the original image but cropped)
res(a:b,c:d) = BW * 255;

% We concatenate the three channels for a black and white image
canv = cat(3, res, res, res);
R_out = canv(:,:,1); % Red channel
G_out = canv(:,:,2); % Green channel
B_out = canv(:,:,3); % Blue channel

y = R_out, G_out, B_out, ampli_f, ampli_r;