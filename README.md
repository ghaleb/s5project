# S5 Project

Audio support for navigating an obstacle course for vision impaired people

## Requirements 
- Raspberry Pi
- Camera module for Raspberry Pi
- Raspberry Pi Juice
- MATLAB R2022b
- Wired headphones (Jack 3.5mm)

## Assemble
1. Insert the Raspberry Pi in the lower part of the case (which has two holes).
2. Insert the camera into the hole from the outside and place it on the bracket.
3. Plug the camera into the dedicated port on the Raspberry.
4. Insert the battery on the Raspberry.
5. Press the button on the left to turn on the battery, a DEL should light up.
6. Put the top part of the case on the device with the harness.
7. Plug the headphones into the 3.5mm jack port.

## Install, compile & run
1. To first setup the Raspberry Pi refer to `docs/pi_setup.pdf`.
2. Plug the camera and the battery to the device.
3. Connect the device to the same network as your computer.
4. Once your device is connected, note it's IP address. You can look it up by entering `ifconfig` in the terminal.
5. Open `src/Model.slx` with MATLAB R2022b.
    1. Go to **Hardware support** > **Hardware settings** > **Hardware implementation** > **Hardware board settings** > **Target hardware resources** > **Board parameters**
    2. Enter the device's IP address, username, password in **Device address**, **Username** & **Password**.  (By default the password is raspberry)
    3. Apply the changes and click OK.
    4. In **Hardware**, click on **Build** in the Deploy section.
    It should take 1-2mins to build.
6. From the root of the device:
    ```
    cd MATLAB_ws/R2022b/
    sudo ./Model.elf
    ```

### Files
`src/main.m`: Image processing and audio generation function  
`src/Model.slx`: Simulink Model containing, image processing, audio generation & video display  
`docs/Report.pdf`: Project report  
`reports/`: Meeting notes for weekly reports  
`assets/bottom.3mf`: 3D Model of bottom part of the case  
`assets/top.3mf`: 3D Model of top part of the case  


### Group 5 (B)
**Tutor:** [Derya Malak] \
**Members:** [Cindy Do], [Marwa Boulaich], [Barthélémy Charlier], [Ahmed Ghaleb], [Aymeric Legros].

### Useful links
[Raspberry Pi 4 Setup Guide]
[Gantt Chart template] 


[Derya Malak]:mailto:Derya.Malak@eurecom.fr
[Cindy Do]: mailto:Cindy.Do@eurecom.fr
[Ahmed Ghaleb]: mailto:Ahmed.Ghaleb@eurecom.fr
[Aymeric Legros]: mailto:Aymeric.Legros@eurecom.fr
[Barthélémy Charlier]:mailto:Barthelemy.Charlier@eurecom.fr
[Marwa Boulaich]: mailto:Marwa.Boulaich@eurecom.fr
[Raspberry Pi 4 Setup Guide]: /docs/pi_setup.pdf
[Gantt Chart template]: https://templates.office.com/en-us/Simple-Gantt-Chart-TM16400962
