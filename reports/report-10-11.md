**Meeting date**: 10/11/2022 @ 11:30

Time: <g>Average</g> \
Scope: <o>Average</o>

### What has been done?
- Start of the poster and the slide presentation
- research on DL start 
- Line detection and tracking 
- Sound in stereo 
### What hasn't been done and why?
- Design for model (do the measures)
- Switch on simulink in progress
- 3D sound (to reconsider)
> Action plan: Resolve reason
### What needs to be done?
- Connect audio output to video input 
- Video treatment optimization
- Start finding visuals 
- Measures for raspberry
- Contact association for visually impaired people on audio feedback 
