**Meeting date**: DD/MM/YYYY @ HH:MM

Time: <g>Good</g> \
Scope: <o>Average</o>

### What has been done?
- Task 1
- Task 2
### What hasn't been done and why?
- Task 1: Reason
- Task 2: Reason
> Action plan: Resolve reason
### What needs to be done?
- Task 1
- Task 2

<style>
r { color: Red }
o { color: Orange }
g { color: Green }
</style>