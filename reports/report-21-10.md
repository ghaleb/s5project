**Meeting date**: 21/10/2022 @ 16:49

**Absents**: \
Mardi: Barthélémy \
Vendredi: Marwa & Barthélémy

Time: Average \
Scope: Average

### What has been done?
- Detection image black and white
- Sound 2D 
- Research on deep learning : not finished yet 
- Latex and layout
- Measurements of the Raspberry with the camera  
### What hasn't been done and why?
- 3D Sound: Material lacking
- Research on DL: not done yet
> Action plan: 
### What needs to be done?
- Research on DL
- Material for 3D Sound
- Design for model
- Program instruction
- Line detection and tracking

Notes with tutor:
She will come Tuesday morning to check 
How we will be evaluated ? - Weekly report will maybe be looked at
- Will we have to follow only one line or could there be 2 lines that are crossing for example ? Will ask Jean Luc
- Audio : For now can only indicate right and left but should we add more angle detection ? 
- What poster should look like ? general explanation of the issue, main work, summary... 
Don't use laTeX for the poster, using powerpoint is easier   
