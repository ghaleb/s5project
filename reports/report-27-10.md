**Meeting date**: 27/10/2022 @ 11:55

**Absents**: 
Jeudi: Marwa, Cindy & Barthélémy.


Time: Average \
Scope: Bad

### What has been done?
- Design for model
- Sound 2D
- Image/video processing
### What hasn't been done and why?
- Program instruction: Switch on simulink
- Research on DL: Absence
- Line detection and tracking: Not finished
- Latex and layout: Absence
- 3D Sound: In progress
### What needs to be done?
- 2D/3D Sound Simulink
- Line detection, crosshair placement
- (Bonus) Make the system work at least once by la rentrée.

