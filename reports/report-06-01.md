**Meeting date**: 06/01/2023 @ 16:30

**Absents**: 
Mardi: Marwa Boulaich, Cindy Do
Jeudi: Marwa Boulaich
Most of us were coming to school late this week.

Time: Bad
Scope: Average

### What has been done?
- Part of final program
- First prototype test
- Contact an association
### What hasn't been done and why?
- Report (work in progress, still intro)
- Poster (wip)
- Presentation (wip)
### What needs to be done?
- Reprint model
- Finish details in program
- Report
- Poster
- Presentation