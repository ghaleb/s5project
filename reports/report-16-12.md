**Meeting date**: 16/12/2022 @ 16:00

**Absents**: 
Barthélémy Charlier, Marwa Boulaich, Cindy Do

Time: Bad
Scope: Bad

### What has been done?
- Program instruction: Switch on simulink (Ask Chiara)
- First prototype printing
### What hasn't been done and why?
- REPORT: the whole team (absent)
- Keep going on LateX (absent)
- Contact a visual impaired association (give up)
### What needs to be done?
- Finish program
- Report
- Poster for 18th of january
- Instruction manual 
- Reprint model with new changes