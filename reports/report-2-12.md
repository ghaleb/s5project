**Meeting date**: 02/12/2022 @ 15:40

**Absents**: /


Time: Bad \
Scope: Average

### What has been done?
- Line detection and tracking
- Latex and layout: in process
### What hasn't been done and why?
- EXAM PERIOD
- Design for model (forgotten)
- Program instruction: Switch on simulink (lack of knowledge)
- Research on DL: Abortion
- 3D Sound: Abortion
### What needs to be done?
- (Bonus) Make the system work at least once by the year
- Program instruction: Switch on simulink
- REPORT: Barthélémy
- Keep going on LateX
- Measurements of Raspberry 