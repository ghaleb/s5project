**Meeting date**: 24/01/2023 @ 12:00

**Absents**: 
Mercredi 11/01/2023: Barthélémy Charlier (Late), Marwa Boulaïch (Late), Cindy Do (Late)
Jeudi 12/01/2023: Barthélémy Charlier, Ahmed Ghaleb

Mardi: All good.
Mercredi 18/01/2023: Barthélémy Charlier (ComProg), Ahmed and Aymeric late.
Jeudi 19/01/2023: Barthélémy Charlier, Marwa Boulaïch, Cindy Do.

Mardi 24/01/2023: Barthélémy Charlier, Marwa Boulaïch late.

Time: Horrible
Scope: Average

### What has been done?
- Part of final program
- First prototype test
- Poster
- Presentation
- 3/4 of the report
- Model
### What hasn't been done and why?
- Report (No work from the one who has to do it)
- Harness
- Line detection

### What needs to be done?
- Finish presentation
- Finish report
- Finish program
- Prepare the presentation