
**Meeting date**: 06/10/2022 @ HH:MM

Time: <g>Good</g> \
Scope: <o>Average</o>

### What has been done?
- OS Configuration 4/5
- Task 2
### What hasn't been done and why?
- OS configuration Barthélémy because of SD card
- Camera set up: technical problem
> Action plan:
             Wait for answers for camera
             contact for SD card
### What needs to be done?
- WBS 
- Tranférer ce qui a été fait sur excel sur Tredo
- Avancer le design de l'hardware
- Lister les tasks et se les partager, établir le gant
- Fixer des dates/ Reprendre le planning : Mettre les vacances et les examens
- Poser les questions sur les consignes
- 

<style>
r { color: Red }
o { color: Orange }
g { color: Green }
</style>
