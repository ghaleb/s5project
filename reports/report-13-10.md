**Meeting date**: 13/10/2022 @ 11:30

Time: Average \
Scope: Average

### What has been done?
- [x] OS Configuration 5/5
- [x] Camera setup
- [x] Transférer ce qui a été fait sur excel sur Trello
- [x] Fixer des dates/ Reprendre le planning : Mettre les vacances et les examens
### What hasn't been done and why?
- WBS: Absence
- Avancer le design de l'hardware: manque d'effectif
- Lister les tasks et se les partager, établir le gant: à finir
- Poser les questions sur les consignes: Pas de prise de rdv
> Action plan: Les absents font le WBS et le gant,
 prendre un rdv avec le tuteur pour jeudi au plus tard.
### What needs to be done?
- Detection image avec filtre haute fréquence et N&B
- Son en 3D
- Recherche sur DL, Hough
- LaTeX et mise en page
- Mesures Raspberry avec la camera