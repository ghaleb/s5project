**Meeting date**: 08/12/2022 @ 11:55

**Absents**: 
Barthélémy Charlier


Time: Bad
Scope: Average

### What has been done?
- Measurements of Raspberry 
- Design of the hard drive
- Latex and layout in process
- Ask for the template
### What hasn't been done and why?
-  EXAM PERIOD
- Program instruction: Switch on simulink (technical problem)
- REPORT: Barthélémy (absent)
### What needs to be done?
- (Bonus) Make the system work at least once by the year
- Program instruction: Switch on simulink (Ask Chiara)
- REPORT: the whole team
- Keep going on LateX
- Contact a visual impaired association